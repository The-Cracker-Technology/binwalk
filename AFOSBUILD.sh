rm -rf /opt/ANDRAX/binwalk

apt update

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Apt update... PASS!"
else
  # houston we have a problem
  exit 1
fi

apt install -y git locales build-essential qtbase5-dev mtd-utils gzip bzip2 tar arj lhasa p7zip p7zip-full cabextract cramfsswap squashfs-tools zlib1g-dev liblzma-dev liblzo2-dev sleuthkit default-jdk lzop srecord cpio

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Apt install required packages... PASS!"
else
  # houston we have a problem
  exit 1
fi

DEFAULTDIR=$(pwd)

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/pip3 install setuptools matplotlib capstone pycryptodome gnupg tk

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip3 install requirements... PASS!"
else
  # houston we have a problem
  exit 1
fi

git clone --quiet --depth 1 --branch "master" https://github.com/devttys0/yaffshiv
(cd yaffshiv && /opt/ANDRAX/PYENV/python3/bin/python3 setup.py install)

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Yaffshiv install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $DEFAULTDIR

git clone --quiet --depth 1 --branch "master" https://github.com/devttys0/sasquatch
(cd sasquatch && wget https://patch-diff.githubusercontent.com/raw/devttys0/sasquatch/pull/47.patch && patch -p1 < 47.patch && CFLAGS=-fcommon ./build.sh)

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Sasquatch install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $DEFAULTDIR

git clone --quiet --depth 1 --branch "master" https://github.com/sviehb/jefferson
(cd jefferson && /opt/ANDRAX/PYENV/python3/bin/python3 -mpip install -r requirements.txt && /opt/ANDRAX/PYENV/python3/bin/python3 setup.py install)

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Jefferson... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $DEFAULTDIR

TIME=`date +%s`
INSTALL_LOCATION=/usr/local/bin

git clone --quiet --depth 1 --branch "master" https://github.com/npitre/cramfs-tools
(cd cramfs-tools && make && install mkcramfs $INSTALL_LOCATION && install cramfsck $INSTALL_LOCATION)

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Cramfs-tools install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $DEFAULTDIR

git clone --quiet --depth 1 --branch "main" https://github.com/jrspruitt/ubi_reader
(cd ubi_reader && /opt/ANDRAX/PYENV/python3/bin/pip3 install .)

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Ubi_reader... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $DEFAULTDIR

/opt/ANDRAX/PYENV/python3/bin/pip3 install .

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip3 install local package... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
